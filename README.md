# js-calendar

Les contraintes sont les suivantes :

###  Conception

- Réalisation de diagrammes de Use Case en se basant sur les users stories (à voir en dessous)
- Réalisation d'un diagramme de classe
- Réalisation de maquettes (wireframe)

###  Technique

- Utilisation de Npm et Webpack
- Utilisation de la librairie moment.js pour la gestion des dates ( https://momentjs.com/ )
- Création d'au moins une classe
- Application responsive (bootstrap)
- Documenter votre code : Mettre la JS Doc sur toutes les fonctions/méthodes pour dire ce qu'elles sont sensées faire)
- Utilisation de git/gitlab pour le travail en groupe : Créer les milestones, lister les tâches, assigner les tâches, travailler sur un dépôt commun.

Vous réaliserez également un README avec vos différents diagrammes commentés et une petite présentation du projet.

##  User Stories

1. En tant qu'utilisateur.ice, je veux pouvoir visualiser tous mes événements du mois en cours afin de pouvoir m'organiser à court terme

2. En tant qu'utilisateur.ice, je veux pouvoir créer de nouveaux événements afin de ne pas oublier de futurs rendez-vous

3. En tant qu'utilisateur.ice, je veux pouvoir modifier les événements existants afin de pouvoir annuler ou reporter certains d'entre eux

4. En tant qu'utilisateur.ice, je veux pouvoir donner un code couleur à mes évènements afin d'avoir une meilleure visualisation des évènements à venir.

   

## Visuels :

### Maquette Fonctionnelle :

![](./img/maquette2.png)



### UML :

#### Use Case Diagram:

![](./img/UseCaseDiagram.png)

####  Class Diagram:

![](./img/CalendarClassDiagram.png)



