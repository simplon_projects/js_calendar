/// <reference types="Cypress" />

describe('Calendar interface test', () => {

    it('should display event div', () => {

        cy.visit('http://localhost/projets-simplon/js-calendar/');

        cy.get('#test').click();
        cy.get('#placeEvent').should('be.visible')

    });

    it('should add an instance of Event', () => {
        cy.get('#sendBtn').click();
        cy.get('#test').should('contain', 'div');
    });

})