import { Calendar } from "./Calendar";
import moment from "moment";
console.log('bloup');

// localStorage.setItem('test', JSON.stringify(moment()));

// On crée une nouvelle instance de la classe Calendrier
let calendar = new Calendar();

// Puis on dessine le calendrier du mois en cours (méthode draw() de la classe Calendar)
calendar.draw();

/* DISPLAY EVENT FORM on click on a date
/* on crée un array dateCells qui sélectionne toutes les cases du tableau calendrier
/* Puis on fait une une boucle sur toutes ces cases (sauf si elles sont vides) pour qu'au click on affiche le formulaire
*/
let dateCells = document.querySelectorAll("td");

for (let i = 0; i < dateCells.length; i++) {
    if (dateCells[i].innerHTML !== "") {
        dateCells[i].addEventListener("click", () => {

            calendar.formDraw(dateCells[i]);
        });
    };
}
// AJOUT D'UN NOUVEL EVENEMENT VIA LE FORMULAIRE
calendar.eventsDraw();


/* Next Month Button: Au click sur la flèche 'next',
/* on redessine le calendrier avec une date M+1 (grâce à l'incrémentation du compteur)
 */
let nextM = document.querySelector("#next");
nextM.addEventListener("click", () => {
    calendar.counter += 1;
    calendar.draw(calendar.counter);
});

/* Last Month Button : Au click que la flèche 'previous', 
/* on redessine le calendrier avec une date M-1 (grâce à la décrémentation du compteur)
 */
let lastM = document.querySelector("#prev");
lastM.addEventListener("click", () => {
    calendar.counter -= 1;
    calendar.draw(calendar.counter);
});