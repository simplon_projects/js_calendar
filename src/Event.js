import { calendarFormat } from "moment";

export class Event {
    /**
     *
     * @param {string} name nom de l'événement
     * @param {date} dateBegin date de début de l'évènement
     * @param {date} dateEnd date de fin de l'évènement
     * @param {string} place lieu de l'évènement
     * @param {string} type type d'évènement
     */
    constructor(name, dateBegin, dateEnd, place, type) {
        this.name = name;
        this.dateBegin = dateBegin;
        this.dateEnd = dateEnd;
        this.place = place;
        this.type = type;
    }

}
