import moment from "moment";
import { Event } from "./Event";

export class Calendar {


    /**
     * @param {number} counter to pass the months
     * @param {Event[]} events an array of events
     */
    constructor() {
        this.counter = 0;
        // this.events.map(item => item.description = 'bloup');
        this.events = [];
    }

    draw(counter) {

        let m = moment().add(counter, 'month');

        let header = document.querySelector('#header');
        header.innerHTML = m.format('MMMM YY');

        let cells = document.querySelectorAll('td');

        let daysInMonth = m.daysInMonth();
        // On crée un tableau dans lequel on va mettre toutes les valeurs de dates (DD) selon le mois
        let dates = [];
        for (let i = 1; i <= daysInMonth; i++) {
            dates.push(i);
        }

        for (let i = 0; i < cells.length; i++) {
            cells[i].innerHTML = '';
        }

        // On met dans une variable le premier jour du mois
        let firstDayOfMonth = m.startOf('month').format('dddd');

        // On fait une boucle qui va dessiner tous les jours du mois
        for (let i = 0; i < dates.length; i++) {
            // On conditionne la case où commencer à dessiner selon le 1er jour du mois 
            switch (firstDayOfMonth) {
                case 'Sunday':
                    cells[i].innerHTML = dates[i];
                    break;
                case 'Monday':
                    cells[i + 1].innerHTML = dates[i];
                    break;
                case 'Tuesday':
                    cells[i + 2].innerHTML = dates[i];
                    break;
                case 'Wednesday':
                    cells[i + 3].innerHTML = dates[i];
                    break;
                case 'Thursday':
                    cells[i + 4].innerHTML = dates[i];
                    break;
                case 'Friday':
                    cells[i + 5].innerHTML = dates[i];
                    break;
                case 'Saturday':
                    cells[i + 6].innerHTML = dates[i];
                    break;
            }
        }
    }

    formDraw(selectedDay) {

        let formEvent = document.querySelector("#placeEvent");
        formEvent.style.display = "block";

        // pour mettre la date cliquée dans le formulaire a startDate
        let startDate = document.querySelectorAll("#dateBegin"); // on selectionne la case du formulaire à startDate
        let clickedMonth = document.querySelector("#header"); // On sélectionne le mois et l'année affichés (mmmm YY)

        let clickedDay = selectedDay.innerHTML; // On selectionne la valeur du jour clické (DD)

        // On va devoir spliter le mois et l'année récupérés précedemment
        let actualMonthAndYear = clickedMonth.innerText; // on met la string complète dans une variable
        let actuMonthYear = actualMonthAndYear.split(" "); // On split la string et on met les 2 éléments obtenus dans un array ['mmmm', 'YY']

        let clickedDate; // On déclare une variable dans la quelle on mettra la date au format d'un objet moment()
        clickedDate = moment(
            clickedDay + "/" + actuMonthYear[0] + "/" + actuMonthYear[1],
            "D/MMMM/YY"
        );

        // Et on renvoie cet objet dans le bon format pour la case du formulaire 'startDate'
        // console.log(clickedDate.format('Y-MM-DD'));
        startDate[0].value = clickedDate.format("Y-MM-DD");
    };


    eventsDraw(event) {

        // for (let index = 0; index < this.events.length; index++) {
        //     localStorage.setItem(index, this.event[index]);
        // };

        // localStorage.setItem('events', this.events[]);


        // localStorage.getItem('events')

        let submitEvent = document.querySelector("#sendBtn"); // On sélectionne le bouton d'ajout d'évènement
        let dateCells = document.querySelectorAll("td");

        submitEvent.addEventListener("click", () => {
                                                      let eventName = document.querySelector(
                                                        "#event-name"
                                                      ).value;
                                                      let startDate = document.querySelector(
                                                        "#dateBegin"
                                                      );
                                                      let endDate = document.querySelector(
                                                        "#dateEnd"
                                                      ).value;
                                                      let eventPlace = document.querySelector(
                                                        "#event-place"
                                                      ).value;
                                                      let eventType = document.getElementById(
                                                        "event-type"
                                                      ).options[
                                                        document.getElementById(
                                                          "event-type"
                                                        ).selectedIndex
                                                      ].text;

                                                      //Empty form values
                                                      document.querySelector(
                                                        "#event-name"
                                                      ).value = "";
                                                      document.querySelector(
                                                        "#event-place"
                                                      ).value = "";
                                                      document.getElementById(
                                                        "event-type"
                                                      ).selectedIndex = "0";

                                                      // On utilise la methode JSON.stringify pour pouvoir garder l'objet crée en localstorage
                                                      let newEvent = JSON.stringify(
                                                        new Event(
                                                          eventName,
                                                          startDate.value,
                                                          endDate,
                                                          eventPlace,
                                                          eventType
                                                        )
                                                      );

                                                      // On attribue une div à ce nouvel event pour l'afficher
                                                      let eventDiv = document.createElement(
                                                        "div"
                                                      );
                                                      eventDiv.classList.add(
                                                        "eventDiv"
                                                      );

                                                      // On fait une boucle pôur récupérer la 'td' dans laquelle on va appendChild la eventDiv
                                                      for (
                                                        let i = 0;
                                                        i < dateCells.length;
                                                        i++
                                                      ) {
                                                        let date =
                                                          dateCells[i]
                                                            .innerHTML;
                                                        moment(
                                                          startDate.value
                                                        ).format("d");
                                                        let splitDate = startDate.value.split(
                                                          "-"
                                                        );

                                                        if (
                                                          splitDate[2] === date
                                                        ) {
                                                          // Conditions pour les couleurs des events selon eventType
                                                          if (
                                                            eventType ==
                                                            "Anniversary"
                                                          ) {
                                                            eventDiv.classList.add(
                                                              "anniversary"
                                                            );
                                                          }
                                                          if (
                                                            eventType ==
                                                            "Professionnal"
                                                          ) {
                                                            eventDiv.classList.add(
                                                              "professionnal"
                                                            );
                                                          }
                                                          if (
                                                            eventType ==
                                                            "Familial"
                                                          ) {
                                                            eventDiv.classList.add(
                                                              "familial"
                                                            );
                                                          }
                                                          if (
                                                            eventType ==
                                                            "Hobbies"
                                                          ) {
                                                            eventDiv.classList.add(
                                                              "hobbies"
                                                            );
                                                          }
                                                          dateCells[
                                                            i
                                                          ].appendChild(
                                                            eventDiv
                                                          );
                                                        }
                                                      }

                                                      this.events.push(
                                                        newEvent
                                                      ); // On ajoute l'évènement crée dans le tableau des events de Calendar

                                                      //On refait disparaitre le formulaire une fois qu'il a été envoyé
                                                      let formEvent = document.querySelector(
                                                        "#placeEvent"
                                                      );
                                                      formEvent.style.display =
                                                        "none";
                                                    });
    }
}